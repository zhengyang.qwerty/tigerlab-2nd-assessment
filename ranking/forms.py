from django import forms


class UploadMatchForm(forms.Form):
    csv_file = forms.FileField()