from django.urls import path

from . import views

urlpatterns = [
    path("", views.ranking_view, name='ranking'),
    path("upload/", views.upload_view, name='upload'),
    path("match/", views.match_view, name='match'),
    path("update/<int:match_id>/", views.update_matches, name='update_match'),
]