from django.shortcuts import render, get_object_or_404
from .forms import UploadMatchForm
from .utils import read_match_score_csv, sort_ranking, get_all_matches
from .models import Match


def upload_view(request):
    if request.method == 'POST':
        form = UploadMatchForm(request.POST, request.FILES)
        if form.is_valid():
            csv_file = request.FILES['csv_file']
            read_match_score_csv(csv_file)
            ranked_teams_with_rank = sort_ranking()
            return render(request, "successful.html", {"ranking": ranked_teams_with_rank})   
    else:
        form = UploadMatchForm()
    return render(request, "upload.html", {"form": form})


def match_view(request):
    match_lists = get_all_matches()
    return render(request, "match.html", {"match_lists": match_lists})


def ranking_view(request):
    ranked_teams_with_rank = sort_ranking()
    return render(request, "ranking.html", {"ranking": ranked_teams_with_rank})

def update_matches(request, match_id):
    match = get_object_or_404(Match, match_id=match_id)
    if request.method == 'POST':
        match.score_1 = request.POST['score_1']
        match.score_2 = request.POST['score_2']
        match.save()
    ranked_teams_with_rank = sort_ranking()
    return render(request, "ranking.html", {"ranking": ranked_teams_with_rank})
