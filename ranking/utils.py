import csv
from .models import Match


def read_match_score_csv(csv_file):
    decoded_file = csv_file.read().decode('utf-8')
    csv_reader = csv.reader(decoded_file.splitlines())
    for row in csv_reader:
        if len(row) <= 4:
            Match.objects.create(
                team_1 = row[0],
                score_1 = row[1],
                team_2 = row[2],
                score_2 = row[3],
            )


def sort_ranking():
    matches = Match.objects.all()
    teams_point = {}
    for match in matches:
        if match.team_1 not in teams_point:
            teams_point[match.team_1] = 0
        if match.team_2 not in teams_point:
            teams_point[match.team_2] = 0

        if match.score_1 > match.score_2:
            teams_point[match.team_1] += 3
        elif match.score_2 > match.score_1:
            teams_point[match.team_2] += 3
        else:
            teams_point[match.team_1] += 1
            teams_point[match.team_2] += 1

    sorted_ranking = sorted(teams_point.items(), key=lambda x:(-x[1], x[0].lower()))
    ranked_teams_with_rank = [(rank + 1, team, score) for rank, (team, score) in enumerate(sorted_ranking)]
    return(ranked_teams_with_rank)


def get_all_matches():
    matches = Match.objects.all()
    return(matches)
