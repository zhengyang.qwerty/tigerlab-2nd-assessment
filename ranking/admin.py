from django.contrib import admin
from .models import Match


class MatchAdmin(admin.ModelAdmin):
  list_display = ("team_1", "score_1", "team_2", "score_2",)

admin.site.register(Match, MatchAdmin)

