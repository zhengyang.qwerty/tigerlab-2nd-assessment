from django.db import models

class Match(models.Model):
    match_id = models.AutoField(primary_key=True)
    team_1 = models.CharField(max_length=255)
    team_2 = models.CharField(max_length=255)
    score_1 = models.IntegerField()
    score_2 = models.IntegerField()
