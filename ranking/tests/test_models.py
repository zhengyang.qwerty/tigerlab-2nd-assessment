import pytest
from django.core.files.uploadedfile import SimpleUploadedFile
from ranking.models import Match, File


def test_test():
    assert 1 == 1

@pytest.mark.django_db
def test_create_match():
    # Create a Match object
    Match.objects.create(team_1='Team A', team_2='Team B', score_1=1, score_2=1)

    # Check that Match object is created
    assert Match.objects.count() == 1

@pytest.mark.django_db
def test_create_file():
    # Create a File object
    file_content = b'Test file content'
    new_file = File.objects.create(file=SimpleUploadedFile("test.txt", file_content))

    # Check that the file object is created
    assert new_file.pk is not None
    assert new_file.file.read() == file_content
    assert new_file.file.name == 'test.txt'

    # Clean up - Delete the file after the test
    new_file.file.delete()

