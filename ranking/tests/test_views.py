import pytest
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, RequestFactory
from ranking.models import Match
from ranking.views import update_matches, upload_view 


client = Client()

@pytest.mark.django_db
def test_upload_view():
    # Create a test CSV file
    csv_content = "Team A,2,Team B,1"
    csv_file = SimpleUploadedFile("test_match_scores.csv", csv_content.encode())

    # Create a POST request
    request = RequestFactory().post('/upload/', {'csv_file': csv_file}, format='multipart')

    # Call the view function
    response = upload_view(request)

    # Check that the Match object is created
    assert Match.objects.count() == 1
    match = Match.objects.first()
    assert match.team_1 == 'Team A'
    assert match.team_2 == 'Team B'
    assert match.score_1 == 2
    assert match.score_2 == 1

    # Check the response
    assert response.status_code == 200

@pytest.mark.django_db
def test_match_view():
    response = client.get('/ranking/match/')
    assert response.status_code == 200

@pytest.mark.django_db
def test_ranking_view():
    response = client.get('/ranking/')
    assert response.status_code == 200

@pytest.mark.django_db
def test_update_matches_view():
    # Create a test match
    match = Match.objects.create(match_id=1, score_1=0, score_2=0)
    
    # Create a POST request
    updated_scores = {'score_1': 2, 'score_2': 1}
    request = RequestFactory().post('/update_match/1/', data=updated_scores)
    
    # Call the view function
    response = update_matches(request, match_id=1)
    
    # Check that the match scores were updated
    match.refresh_from_db()
    assert match.score_1 == 2
    assert match.score_2 == 1
    
    # Check the response
    assert response.status_code == 200
    assert b'ranking' in response.content

