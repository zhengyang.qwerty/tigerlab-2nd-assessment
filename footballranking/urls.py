from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("ranking/", include("ranking.urls")),
    path('admin/', admin.site.urls),
]
